/* CONFIGURATION */

const OpenVidu = require('openvidu-node-client').OpenVidu;
const OpenViduRole = require('openvidu-node-client').OpenViduRole;

// Check launch arguments: must receive openvidu-server URL and the secret
if (process.argv.length !== 5) {
    console.log("Usage: node " + __filename + " OPENVIDU_URL OPENVIDU_SECRET IS_SERVER");
    process.exit(-1);
}
// For demo purposes we ignore self-signed certificate
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

// Node imports
const express = require('express');
const fs = require('fs');
const https = require('https');
const bodyParser = require('body-parser'); // Pull information from HTML POST (express4)
const app = express(); // Create our app with express

const crypto = require("crypto");


const cors = require('cors');
app.use(cors());

app.use(bodyParser.urlencoded({
    'extended': 'true'
})); // Parse application/x-www-form-urlencoded
app.use(bodyParser.json()); // Parse application/json
app.use(bodyParser.json({
    type: 'application/vnd.api+json'
})); // Parse application/vnd.api+json as json

// Listen (start app with node server.js)
const options = {
    key: fs.readFileSync('openvidukey.pem'),
    cert: fs.readFileSync('openviducert.pem')
};

function errorHandler(err, req, res, next) {
    console.log(`errorHandler ${err}`);
    res.status(500).json({error: err.toString()});
}

const environment = process.argv[4];
console.log(`is server: ${environment}`);

if (environment === 'server') {
    console.log(`start server environment`);
    app.listen(5445);
} else {
    console.log(`start local environment`);
    https.createServer(options, app).listen(5442); //for local run
}

// Environment variable: URL where our OpenVidu server is listening
const OPENVIDU_URL = process.argv[2];
// Environment variable: secret shared with our OpenVidu server
const OPENVIDU_SECRET = process.argv[3];

// Entrypoint to OpenVidu Node Client SDK
const OV = new OpenVidu(OPENVIDU_URL, OPENVIDU_SECRET);

const {body, validationResult} = require('express-validator');

const axios = require('axios');

const kmsApi = axios.create({
    baseURL: 'https://localhost:4443',
    timeout: 10000,
    headers: {'Authorization': getBasicAuth()}
});

const admin = require("firebase-admin");

const serviceAccount = require("./podcasts-d312e-firebase-adminsdk-m80xr-1fcaa4ab9b.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});

let sessions = {};//todo firestore
let nativeSessions = {};

console.log("App listening on port 5442");

/* CONFIGURATION */


/* REST API */

const validateFirebaseIdToken = (req, res, next) => {
    if (!req.headers.authorization) {
        console.error('No Firebase ID token was passed authorization header.',
            'Make sure you authorize your request by providing the following HTTP header:',
            'Authorization: <Firebase ID Token>');
        res.status(403).json({error: 'Unauthorized'});
        return;
    }
    const idToken = req.headers.authorization;
    admin.auth().verifyIdToken(idToken).then(decodedIdToken => {
        console.log('ID Token correctly decoded', decodedIdToken);
        admin.auth().getUser(decodedIdToken.uid).then((userRecord) => {
            req.user = userRecord;
            req.userId = userRecord.uid;
            req.isHost = decodedIdToken.isHost;
            console.log("========= here");
            if (userRecord.customClaims) {
                console.log("========= here 2");
                req.clientId = userRecord.customClaims['clientId']
            }
            next();
        }).catch(error => {
            console.error('Error while getting Firebase User record:', error);
            res.status(403).json({error: 'Unauthorized'});
        });
    }).catch(error => {
        console.error('Error while verifying Firebase ID token:', error);
        res.status(403).json({error: 'Unauthorized'});
    });
};

const validateClientIdHeader = (req, res, next) => {
    if (!req.headers["client-id"]) {
        console.error('No Client-Id provided');
        res.status(403).json({error: 'No Client-Id provided'});
        return;
    }
    req.clientId = req.headers["client-id"];
    next()
};

async function getSession(clientId, sessionId) {
    let session = sessions[sessionId];
    if (!session) {
        //Check if session is stored in Firestore. Get it in case it's stored and status is upcoming
        let storedSessionSnapshot = await loadSession(clientId);
        let storedSession = storedSessionSnapshot.data();
        if (storedSession && storedSession.status === 'upcoming') {
            session = storedSession;
            sessions[storedSession.sessionId] = storedSession;
        }
    }
    return session;
}

async function saveSession(clientId, session) {
    let sessionPath = `clients/${clientId}/sessions/current`;
    await admin.firestore().doc(sessionPath).set(session, {merge: true});
}

async function deleteSession(clientId, sessionId) {
    delete sessions[sessionId];
    delete nativeSessions[sessionId];
    let sessionPath = `clients/${clientId}/sessions/current`;
    await admin.firestore().doc(sessionPath).delete();
}

async function loadSession(clientId) {
    let sessionPath = `clients/${clientId}/sessions/current`;
    return await admin.firestore().doc(sessionPath).get();
}

async function periodicTask() {
    for (const [sessionId, session] of Object.entries(sessions)) {
        let nativeSession = nativeSessions[sessionId];
        if (!nativeSession) {
            if (session.status === "live") {//just in case. Idk how it can happen
                await deleteSession(session.clientId, sessionId)
            }
            return;
        }
        try {
            await nativeSession.sessionObject.fetch();
            let activeConnections = nativeSession.sessionObject.activeConnections;
            let currentListeners = session.audience.listeners;
            let listenersCount = activeConnections.length - 1;
            if (listenersCount < 0) listenersCount = 0;
            //check active listeners count
            if (currentListeners !== listenersCount) {
                session.audience.listeners = listenersCount;
                sendEventToSession(session.sessionId, "SESSION_LISTENERS_UPDATED", {
                    amount: listenersCount
                });
            }
        } catch (error) {
            console.log(`Periodic task error ${error}`);
            if (error.message === '404') {
                await deleteSession(session.clientId, sessionId)
            } else {
            }
        }
    }
}

setInterval(periodicTask, 3000);


app.get("/api/getWidgetAppearance",
    validateClientIdHeader,
    function (req, res) {
        let clientId = req.clientId;
        console.log(`/getWidgetAppearence clientId: ${clientId}`);
        let withBranding = !isBrandingFreeClient(clientId);
        res.send({withBranding: withBranding});
    });


app.get("/api/hello",
    function (req, res) {
        res.send({hello: "Hi guys!"});
    });

function isBrandingFreeClient(clientId) {
    return clientId === "RttyD5FdI5r9FZ3ZPVWG";
}

function findSessionByClientId(clientId) {
    let sessionEntry = Object.entries(sessions).find(function (entry) {
        return entry[1].clientId === clientId
    });
    return sessionEntry ? sessionEntry[1] : undefined;
}

app.get("/api/getCurrentSession",
    validateClientIdHeader,
    async function (req, res) {
        try {
            let clientId = req.clientId;
            console.log(`/getCurrentSession clientId: ${clientId}`);
            let sessionEntry = Object.entries(sessions).find(function (entry) {
                console.log(`======= ${JSON.stringify(entry[1].clientId)}`);
                return entry[1].clientId === clientId
            });

            if (!sessionEntry) {
                let sessionFromDB = await loadSession(clientId);
                if (!sessionFromDB) {
                    res.status(404).json({error: `No session found for client: ${req.query.clientId}`});
                    return;
                }

                let sessionFromDBData = sessionFromDB.data();

                if (sessionFromDBData && sessionFromDBData.status === 'upcoming') {
                    sessions[sessionFromDBData.sessionId] = sessionFromDBData;
                    res.send(sessionFromDBData);
                    return;
                } else {
                    res.status(404).json({error: `No session found for client: ${req.query.clientId}`});
                    return;
                }
            }

            let session = sessionEntry[1];

            res.send(session);
        } catch (error) {
            res.status(500).send(`error when getting session status: ${error}`);
        }
    });

function sendEventToSession(sessionId, event, data) {
    let nativeSession = nativeSessions[sessionId];
    if (nativeSession) {
        kmsApi.post(`${OPENVIDU_URL}/openvidu/api/signal`, {
            "session": nativeSession.sessionObject.getSessionId(),
            "type": event,
            "data": JSON.stringify(data)
        }).then(response => {
            console.log(event + " signal sent");
        }).catch(error => {
            console.log(event + ` signal error ${error.toString()}`);
        });
    }
}

app.post("/api/startSession",
    body("sessionId").not().isEmpty(),
    validateFirebaseIdToken,
    validateClientIdHeader,
    function (req, res) {
        console.log(`/startSession`);

        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.status(400).json({errors: errors.array()});
            return;
        }

        let sessionId = req.body.sessionId;

        let cachedSession = sessions[sessionId];

        if (!cachedSession) {
            res.status(404).send("session not found");
            return;
        }

        if (cachedSession.status === 'live') {
            res.status(403).send("session already live");
            return;
        }


        // New session
        console.log('New session ' + sessionId);

        // Create a new OpenVidu Session asynchronously
        OV.createSession()
            .then(session => {

                // Store a new empty array in the collection of tokens
                nativeSessions[sessionId] = {};
                nativeSessions[sessionId].tokens = [];
                nativeSessions[sessionId].sessionObject = session;

                let serverData = JSON.stringify({userId: req.body.userId, isHost: true});
                console.log("here2");

                let connectionProperties = {
                    data: serverData,
                    role: OpenViduRole.PUBLISHER
                };

                // Generate a new connection asynchronously with the recently created connectionProperties
                session.createConnection(connectionProperties)
                    .then(connection => {

                        // Store the new Session in the collection of Sessions
                        sessions[sessionId].status = "live";
                        saveSession(req.clientId, sessions[sessionId]);
                        sessions[sessionId].startTime = new Date(new Date().toUTCString());
                        sessions[sessionId].host.voiceSessionId = connection.token;

                        // Store the new token in the collection of tokens
                        nativeSessions[sessionId].tokens.push(connection.token);

                        let response = {...sessions[sessionId]};
                        response.voiceSessionId = connection.token;

                        // Return the Token to the client
                        res.status(200).send(response);
                    })
                    .catch(error => {
                        res.status(500).send("error while creating connection");
                        console.error(JSON.stringify(error));
                    });
            })
            .catch(error => {
                console.error(JSON.stringify(error));
                res.status(500).send("error while creating session");
            });
    });

//
// app.post("/openvidu_webhook",
//     function (req, res) {
//         console.log(`/openvidu_webhook`);
//         res.status(200).send({});
//     });

app.post("/api/leaveSession",
    body("sessionId").not().isEmpty(),
    validateFirebaseIdToken,
    validateClientIdHeader,
    async function (req, res) {
        console.log(`/leaveSession`);
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                res.status(400).json({errors: errors.array()});
                return;
            }

            let participantId = req.userId;

            let sessionId = req.body.sessionId;
            let session = await getSession(req.clientId, sessionId);
            let stage = session.stage.find(item => item.id === participantId)

            if (stage) {
                let index = session.stage.indexOf(stage);
                session.stage.splice(index, 1);
                sendEventToSession(sessionId, "STAGE_PARTICIPANT_LEFT", {
                    id: participantId
                })
            }

            let stageRequest = session.audience.stageRequests.find(item => item.id === participantId)

            if (stageRequest) {
                let index = session.audience.stageRequests.indexOf(stageRequest);
                session.audience.stageRequests.splice(index, 1);
                sendEventToSession(sessionId, "REQUEST_STAGE_CANCEL", {
                    id: participantId
                })
            }

            res.status(204).send();
        } catch (error) {
            res.status(500).json({error: `${error.toString()}`});
        }
    });


app.post("/api/joinSession",
    body("sessionId").not().isEmpty(),
    validateFirebaseIdToken,
    validateClientIdHeader,
    async function (req, res) {
        console.log(`/joinSession`);
        try {

            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                res.status(400).json({errors: errors.array()});
                return;
            }

            let sessionId = req.body.sessionId;
            let session = await getSession(req.clientId, sessionId);

            let clientId = req.clientId;

            let isHost = req.isHost;
            let userPath = `clients/${clientId}/${isHost ? 'hosts' : 'listeners'}/${req.userId}`;
            let user = await admin.firestore().doc(userPath).get();
            console.log(`==== user from firebase: ${user}; path: ${userPath}`);

            if (!session) {
                res.status(404).send("session not found");
                return;
            }

            if (session.status !== "live") {
                res.status(404).send("session is not live yet");
                return;
            }

            let nativeSession = nativeSessions[sessionId];
            if (!nativeSession) {
                res.status(404).send("native session not found");
                return;
            }

            // Session already exists
            console.log('Existing session ' + sessionId);

            let serverData = JSON.stringify({participant: req.userId, isHost: isHost});

            let existingStageItem = session.stage.find(item => item.id === req.userId);

            let isPublisher = isHost || (existingStageItem !== undefined);

            let connectionProperties = {
                data: serverData,
                role: isPublisher ? OpenViduRole.PUBLISHER : OpenViduRole.SUBSCRIBER
            };


            // Get the existing Session from the collection
            let existingSession = nativeSession.sessionObject;

            // Generate a new token asynchronously with the recently created connectionProperties
            try {
                let connection = await existingSession.createConnection(connectionProperties)

                // Store the new token in the collection of tokens
                nativeSessions[sessionId].tokens.push(connection.token);

                if (isHost) {
                    session.host.voiceSessionId = connection.token;
                } else if (existingStageItem) {
                    existingStageItem.voiceSessionId = connection.token;
                }

                let response = {...sessions[sessionId]};
                response.voiceSessionId = connection.token;

                //check for online
                await nativeSession.sessionObject.fetch();

                let activeConnections = nativeSession.sessionObject.activeConnections;
                let isHostOnline = activeConnections.some(activeConnection => {
                    return session.host.voiceSessionId === activeConnection.token
                });
                response.host.online = isHostOnline || false;
                response.stage.forEach(stageItem => {
                    let isStageParticipantOnline = activeConnections.some(activeConnection => {
                        return stageItem.voiceSessionId === activeConnection.token
                    });
                    stageItem.online = isStageParticipantOnline || false;
                });
                // Return the token to the client
                res.status(200).send(response);
            } catch (error) {
                console.error(error);
                res.status(500).json({error: `error when try to create connection: ${error}`});
            }
        } catch
            (error) {
            res.status(500).json({error: `error when try to join session ${error}`});

        }

    })
;

app.post("/api/createSession",
    body("startTime").not().isEmpty(),
    body("startTime").isISO8601(),
    body("topic").not().isEmpty(),
    validateFirebaseIdToken,
    validateClientIdHeader,
    async function (req, res) {
        try {
            console.log(`/createSession ${req.userId} ${req.clientId}`);

            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                res.status(400).json({errors: errors.array()});
                return;
            }
            let session = req.body;
            session.clientId = req.clientId;

            let existingSession = Object.entries(sessions).reverse().find(function (entry) {
                return entry[1].clientId === req.clientId
            });

            if (existingSession) {
                res.status(409).json({error: "session for this client already created"})
                return;
            }

            const sessionId = crypto.randomBytes(16).toString("hex");
            session.sessionId = sessionId;
            session.status = "upcoming";
            session.audience = {listeners: 0, stageRequests: []};
            session.stage = [];

            let user = await admin.firestore().doc(`clients/${req.clientId}/hosts/${req.userId}`).get();
            console.log(`clients/${req.clientId}/hosts/${req.userId}`);

            session.host = {
                id: req.userId,
                name: user.data().name,
                photoUrl: user.data().photoUrl,
                muted: false,
                online: false
            };

            sessions[sessionId] = session;
            await saveSession(req.clientId, session);

            res.status(200).send(session);
        } catch (err) {
            console.error(err);
            res.status(500).send({error: `error when try to create session ${err.toString()}`});
        }
    });

app.delete("/api/releaseSession",
    body("sessionId").not().isEmpty(),
    validateFirebaseIdToken,
    validateClientIdHeader,
    async function (req, res) {
        console.log(`/releaseSession`);
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                res.status(400).json({errors: errors.array()});
                return;
            }

            let session = await getSession(req.clientId, req.body.sessionId);

            if (!session) {
                res.status(404).json({error: "session not exist"});
                return;
            }

            let hostId = session.host.id;
            let isHost = req.isHost;
            if (!isHost || hostId !== req.userId) {
                res.status(403).json({error: "user doesn't have permissions"});
                return;
            }

            let clientId = req.clientId;
            let nativeSession = nativeSessions[req.body.sessionId];

            async function closeNativeSession() {
                try {
                    await nativeSession.sessionObject.close();
                    console.log("session closed " + req.body.sessionId)

                    res.status(204).send();
                } catch (error) {
                    if (error.message !== "404") {
                        res.status(500).json({error: "cannot close session " + error.toString()});
                    } else {
                        res.status(409).json({error: `session is auto-closed ${error.toString()}`});
                    }
                }
            }

            if (nativeSession) {
                try {
                    await kmsApi.post(`${OPENVIDU_URL}/openvidu/api/signal`, {
                        "session": nativeSession.sessionObject.getSessionId(),
                        // "to": ["con_Xnxg19tonh", "con_TNVdbuQCJF"],
                        "type": "SESSION_RELEASED",
                        "data": JSON.stringify({
                            just: "should-be-some-data"
                        })
                    });
                    console.log("SESSION_RELEASED signal sent");
                    await closeNativeSession();
                } catch (error) {
                    console.log(`SESSION_RELEASED signal error ${error.toString()}`);
                    await closeNativeSession();
                }
            } else {
                res.status(204).send();
            }
        } catch (error) {
            res.status(500).json({error: `cannot release session: ${error}`});
        } finally {
            await deleteSession(req.clientId, req.body.sessionId);
        }
    });

app.delete("/api/rejectStageRequest",
    body("sessionId").not().isEmpty(),
    body("participantId").not().isEmpty(),
    validateFirebaseIdToken,
    validateClientIdHeader,
    async function (req, res) {
        console.log(`/rejectStageRequest`);

        if (!sessions[req.body.sessionId]) {
            res.status(404).json({error: "session not exist"});
            return;
        }

        let session = sessions[req.body.sessionId]
        let hostId = session.host.id;
        let isHost = req.isHost;
        if (!isHost || hostId !== req.userId) {
            res.status(403).json({error: "user doesn't have permissions"});
            return;
        }

        await _rejectStageRequest(req, res, "REQUEST_STAGE_REJECT");
        res.status(204).send();
    });

app.delete("/api/removeStageRequest",
    body("sessionId").not().isEmpty(),
    body("participantId").not().isEmpty(),
    validateFirebaseIdToken,
    validateClientIdHeader,
    async function (req, res) {
        console.log(`/removeStageRequest`);

        let userId = req.userId;
        let participantId = req.body.participantId;

        if (userId !== participantId) {
            res.status(403).json({error: "user doesn't have permissions"});
            return;
        }

        await _rejectStageRequest(req, res, "REQUEST_STAGE_CANCEL");
        res.status(204).send();
    });

async function _rejectStageRequest(req, res, event) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({errors: errors.array()});
        return;
    }

    let sessionId = req.body.sessionId;
    let session = await getSession(req.clientId, sessionId);

    if (!session) {
        res.status(404).json({error: "session not exist"});
        return;
    }

    let participantId = req.body.participantId;
    let stageRequest = session.audience.stageRequests.find(item => item.id === participantId)

    if (!stageRequest) {
        res.status(404).json({error: "stage request not exist"});
        return;
    }

    let index = session.audience.stageRequests.indexOf(stageRequest);
    session.audience.stageRequests.splice(index, 1);

    let nativeSession = nativeSessions[sessionId];
    if (nativeSession) {
        kmsApi.post(`${OPENVIDU_URL}/openvidu/api/signal`, {
            "session": nativeSession.sessionObject.getSessionId(),
            "type": event,
            "data": JSON.stringify({
                id: participantId
            })
        }).then(response => {
            console.log(event + " signal sent");
        }).catch(error => {
            console.log(event + ` signal error ${error.toString()}`);
        });
    }
}

app.delete("/api/kickParticipantFromStage",
    body("sessionId").not().isEmpty(),
    body("participantId").not().isEmpty(),
    validateFirebaseIdToken,
    validateClientIdHeader,
    function (req, res) {
        console.log(`/kickParticipantFromStage`);

        let session = sessions[req.body.sessionId];
        if (!session) {
            res.status(404).json({error: "session not exist"});
            return;
        }

        let hostId = session.host.id;
        let isHost = req.isHost;
        if (!isHost || hostId !== req.userId) {
            res.status(403).json({error: "user doesn't have permissions"});
            return;
        }

        let participantId = req.body.participantId;
        let sessionId = req.body.sessionId;

        let stageItem = session.stage.find(item => item.id === participantId);

        if (!stageItem) {
            res.status(404).json({error: "stage participant not exist"});
            return;
        }

        let index = session.stage.indexOf(stageItem);
        session.stage.splice(index, 1);

        let nativeSession = nativeSessions[sessionId];
        if (nativeSession) {
            kmsApi.post(`${OPENVIDU_URL}/openvidu/api/signal`, {
                "session": nativeSession.sessionObject.getSessionId(),
                "type": "STAGE_PARTICIPANT_KICKED",
                "data": JSON.stringify({
                    id: participantId
                })
            }).then(response => {
                console.log("STAGE_PARTICIPANT_KICKED" + " signal sent");
            }).catch(error => {
                console.log("STAGE_PARTICIPANT_KICKED" + ` signal error ${error.toString()}`);
            });
        }

        res.status(204).send();
    });

app.post("/api/leaveStage",
    body("sessionId").not().isEmpty(),
    validateFirebaseIdToken,
    validateClientIdHeader,
    async function (req, res) {
        console.log(`/leaveStage`);
        try {
            let sessionId = req.body.sessionId;
            let participantId = req.userId;
            let session = await getSession(req.clientId, sessionId);
            if (!session) {
                res.status(404).json({error: "session not exist"});
                return;
            }

            let stageItem = session.stage.find(item => item.id === participantId);

            if (!stageItem) {
                res.status(404).json({error: "stage participant not exist"});
                return;
            }

            let index = session.stage.indexOf(stageItem);
            session.stage.splice(index, 1);

            let nativeSession = nativeSessions[sessionId];
            if (nativeSession) {
                kmsApi.post(`${OPENVIDU_URL}/openvidu/api/signal`, {
                    "session": nativeSession.sessionObject.getSessionId(),
                    "type": "STAGE_PARTICIPANT_LEFT",
                    "data": JSON.stringify({
                        id: participantId
                    })
                }).then(response => {
                    console.log("STAGE_PARTICIPANT_LEFT" + " signal sent");
                }).catch(error => {
                    console.log("STAGE_PARTICIPANT_LEFT" + ` signal error ${error.toString()}`);
                });
            }

            res.status(204).send();
        } catch (error) {
            res.status(500).json({error: `cannot left: ${error}`});
        }
    });

app.post("/api/muteParticipant",
    body("sessionId").not().isEmpty(),
    body("participantId").not().isEmpty(),
    validateFirebaseIdToken,
    validateClientIdHeader,
    async function (req, res) {
        console.log(`/muteParticipant`);
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                res.status(400).json({errors: errors.array()});
                return;
            }

            const sessionId = req.body.sessionId;

            if (!sessions[sessionId]) {
                res.status(404).json({error: "session not found"});
                return;
            }

            let participantId = req.body.participantId;

            let nativeSession = nativeSessions[sessionId];
            let nativeSessionId = nativeSession.sessionObject.getSessionId();

            console.log(`/muteParticipant native session ID: ${nativeSessionId}`);

            let byHost = req.isHost;

            try {
                let response = await kmsApi.post(`${OPENVIDU_URL}/openvidu/api/signal`, {
                    "session": nativeSessionId,
                    // "to": ["con_Xnxg19tonh", "con_TNVdbuQCJF"],
                    "type": "MUTE",
                    "data": JSON.stringify({
                        id: participantId,
                        byHost: byHost || false
                    })
                });

                if (response.status === 200) {
                    let data = response.data;

                    let session = await getSession(req.clientId, sessionId);
                    let stageParticipant = session.stage.find(item => {
                        return item.id === participantId;
                    });

                    if (!stageParticipant) {
                        if (req.isHost && req.userId === participantId) {
                            session.host.muted = true;
                        } else {
                            res.status(404).json({error: "No stage participant found"});
                            return;
                        }
                    } else {
                        stageParticipant.muted = true;
                        stageParticipant.mutedByHost = byHost || false;
                    }
                }

                console.log(`/muteParticipant native response: ${JSON.stringify(response.data)}`);
                res.status(204).send();
            } catch (error) {
                let response = error.response;
                res.status(response.status).send();
            }
        } catch (error) {
            res.status(500).json({error: `Cannot mute participant. ${error.toString()}`});
        }

    })

app.post("/api/unmuteParticipant",
    body("sessionId").not().isEmpty(),
    body("participantId").not().isEmpty(),
    validateFirebaseIdToken,
    validateClientIdHeader,
    async function (req, res) {
        console.log(`/unmuteParticipant`);
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                res.status(400).json({errors: errors.array()});
                return;
            }

            const sessionId = req.body.sessionId;

            if (!sessions[sessionId]) {
                res.status(404).json({error: "session not found"});
                return;
            }

            let participantId = req.body.participantId;

            let nativeSession = nativeSessions[sessionId];
            let nativeSessionId = nativeSession.sessionObject.getSessionId();

            console.log(`/unmuteParticipant native session ID: ${nativeSessionId}`);

            let byHost = req.isHost;

            try {
                let response = await kmsApi.post(`${OPENVIDU_URL}/openvidu/api/signal`, {
                    "session": nativeSessionId,
                    "type": "UNMUTE",
                    "data": JSON.stringify({
                        id: participantId,
                        byHost: byHost || false
                    })
                });

                if (response.status === 200) {
                    let data = response.data;

                    let session = await getSession(req.clientId, sessionId);
                    let stageParticipant = session.stage.find(item => {
                        return item.id === participantId;
                    });

                    if (!stageParticipant) {
                        if (req.isHost && req.userId === participantId) {
                            session.host.muted = false;
                        } else {
                            res.status(404).json({error: "No stage participant found"});
                            return;
                        }
                    } else {
                        stageParticipant.muted = false;
                        stageParticipant.mutedByHost = false;
                    }
                }

                console.log(`/unmuteParticipant native response: ${JSON.stringify(response.data)}`);
                res.status(204).send();
            } catch (error) {
                let response = error.response;
                res.status(response.status).send();
            }
        } catch (error) {
            res.status(500).json({error: `Cannot unmute participant. ${error.toString()}`});
        }

    });

app.post("/api/requestJoiningStage",
    body("sessionId").not().isEmpty(),
    validateFirebaseIdToken,
    validateClientIdHeader,
    async function (req, res) {
        console.log(`/requestJoiningStage`);
        try {

            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                res.status(400).json({errors: errors.array()});
                return;
            }

            const sessionId = req.body.sessionId;

            let session = await getSession(req.clientId, sessionId);
            if (!session) {
                res.status(404).json({error: "session not found"});
                return;
            }

            if (req.isHost) {
                res.status(409).json({error: "Host cannot do this"});
                return;
            }

            let participantId = req.userId;
            let message = req.body.message;

            let nativeSession = nativeSessions[sessionId];
            let nativeSessionId = nativeSession.sessionObject.getSessionId();

            console.log(`/requestJoiningStage native session ID: ${nativeSessionId}`);

            let existingStageRequests = session.audience.stageRequests;

            let requestAlreadySent = existingStageRequests.find((item) => item.participantId === participantId);
            if (requestAlreadySent) {
                res.status(409).json({error: 'Request already sent'});
                return;
            }

            let clientId = req.clientId;

            let userPath = `clients/${clientId}/listeners/${participantId}`;
            let user = await admin.firestore().doc(userPath).get();

            let stageRequest = {
                id: participantId,
                message: message,
                createdAt: new Date(new Date().toUTCString()),
                name: user?.data()?.name,
                photoUrl: user?.data()?.photoUrl
            };
            existingStageRequests.push(stageRequest);

            try {
                let response = await kmsApi.post(`${OPENVIDU_URL}/openvidu/api/signal`, {
                    "session": nativeSessionId,
                    "type": "REQUEST_STAGE_JOIN",
                    "data": JSON.stringify(stageRequest)
                });

                if (response?.status === 200) {
                    let data = response.data;
                }

                console.log(`/requestJoiningStage native response: ${JSON.stringify(response?.data)}`);
                res.status(204).send();
            } catch (error) {
                res.status(500).json({error: `cannot send event ${error.toString()}`});
            }
        } catch (error) {
            res.status(500).json({error: `Cannot request to join stage. ${error.toString()}`});
        }

    });

app.post("/api/addParticipantToStage",
    body("sessionId").not().isEmpty(),
    body("participantId").not().isEmpty(),
    validateFirebaseIdToken,
    validateClientIdHeader,
    async function (req, res) {
        console.log(`/addParticipantToStage`);

        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                res.status(400).json({errors: errors.array()});
                return;
            }

            const sessionId = req.body.sessionId;

            let session = await getSession(req.clientId, sessionId);
            if (!session) {
                res.status(404).json({error: "session not found"});
                return;
            }

            let participantId = req.body.participantId;

            let nativeSession = nativeSessions[sessionId];
            let nativeSessionId = nativeSession.sessionObject.getSessionId();

            try {

                let nativeSession = nativeSessions[sessionId];

                // Get the existing Session from the collection
                let existingSession = nativeSession.sessionObject;

                let serverData = JSON.stringify({userId: participantId, isHost: false});
                console.log("here2");

                let connectionProperties = {
                    data: serverData,
                    role: OpenViduRole.PUBLISHER
                };


                // Generate a new token asynchronously with the recently created connectionProperties
                let connection = await existingSession.createConnection(connectionProperties);

                // Store the new token in the collection of tokens
                nativeSessions[sessionId].tokens.push(connection.token);

                let existingStageRequests = session.audience.stageRequests;

                let existingRequest = existingStageRequests.find((item) => item.id === participantId);

                let response = await kmsApi.post(`${OPENVIDU_URL}/openvidu/api/signal`, {
                    "session": nativeSessionId,
                    // "to": ["con_Xnxg19tonh", "con_TNVdbuQCJF"],
                    "type": "REQUEST_STAGE_APPROVE",
                    "data": JSON.stringify({
                        id: participantId,
                        voiceSessionId: connection.token,
                        name: existingRequest?.name,
                        photoUrl: existingRequest?.photoUrl
                    })
                });

                if (response.status === 200) {
                    let data = response.data;

                    let existingStageRequests = session.audience.stageRequests;

                    let existingRequest = existingStageRequests.find((item) => item.id === participantId);
                    if (existingRequest) {
                        let index = existingStageRequests.indexOf(existingRequest);
                        existingStageRequests.splice(index, 1);
                        existingRequest.muted = false;
                        existingRequest.voiceSessionId = connection.token;
                        session.stage.push(existingRequest);
                    } else {
                        res.status(404).json({error: `Request not exists`});
                        return;
                    }
                }

                console.log(`/addParticipantToStage native response: ${JSON.stringify(response.data)}`);

                res.status(204).send();
            } catch (error) {
                let response = error.response;
                res.status(response.status).send();
            }
        } catch (error) {
            res.status(500).json({error: `Cannot request to addParticipantToStage. ${error.toString()}`});
        }

    });

app.get("/api/getHost",
    validateFirebaseIdToken,
    async function (req, res) {
        console.log(`/getHost`);
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                res.status(400).json({errors: errors.array()});
                return;
            }

            let userId = req.userId;

            let clientId = req.clientId;

            let userPath = `clients/${clientId}/hosts/${userId}`;
            let user = await admin.firestore().doc(userPath).get();

            res.send({
                userId: userId,
                clientId: clientId,
                name: user.data().name,
                photoUrl: user.data().photoUrl,
            });

        } catch (error) {
            res.status(500).json({error: `Cannot request to getHost. ${error.toString()}`});
        }
    });

app.post("/api/hostRegister",
    body("name").not().isEmpty(),
    body("email").not().isEmpty(),
    body("password").not().isEmpty(),
    async function (req, res) {
        try {
            console.log(`/hostRegister`);

            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                res.status(400).json({errors: errors.array()});
                return;
            }

            let name = req.body.name;
            let email = req.body.email;
            let password = req.body.password;

            let user = await admin.auth().createUser({
                email: email,
                password: password,
            });

            let db = admin.firestore();
            let docRef = db.collection('clients').doc();

            await docRef.set({});

            let clientId = docRef.id;

            await admin
                .auth()
                .setCustomUserClaims(user.uid, {isHost: true, clientId: clientId});

            await docRef.collection('hosts').doc(user.uid).set({
                createdAt: admin.firestore.FieldValue.serverTimestamp(),
                name: name,
                clientId: clientId,
                userId: user.uid
            });

            res.status(200).json({clientId: clientId, userId: user.uid, name: name});
        } catch (err) {
            if (err.code === "auth/email-already-exists") {
                res.status(400).json({error: "Email is already taken", type: "email-taken"});
            } else {
                res.status(500).json({error: JSON.stringify(err)});
            }
        }
    });

app.use(errorHandler);


/* REST API */


/* AUXILIARY METHODS */

function getBasicAuth() {
    return 'Basic ' + (Buffer.from('OPENVIDUAPP:' + OPENVIDU_SECRET).toString('base64'));
}

/* AUXILIARY METHODS */
