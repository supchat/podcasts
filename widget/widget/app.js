var openVidu;
var session;

var sessionName;	// Name of the video session the user will connect to
var token;			// Token retrieved from OpenVidu Server

//todo for debug only
window.onParticipantJoined = function (id){};
window.onParticipantStartedSpeaking = function (id){};
window.onParticipantStoppedSpeaking = function (id){};
window.onParticipantLeft = function (id){};
window.onNewEvent = function (id){};
window.onJoined = function (id){};

var publisher;

/* OPENVIDU METHODS */
function joinSession(token, participantId, isPublisher) {

    console.log("join click, token: " + token + ", participantId: " + participantId);

    openVidu = new OpenVidu();
    session = openVidu.initSession();

    // --- 3) Specify the actions when events take place in the session ---

    // On every new Stream received...
    session.on('streamCreated', (event) => {

        // Subscribe to the Stream to receive it
        // HTML video will be appended to element with 'video-container' id
        var subscriber = session.subscribe(event.stream, 'video-container');

        // When the HTML video has been appended to DOM...
        subscriber.on('videoElementCreated', (event) => {

            let id = JSON.parse(subscriber.stream.connection.data.split('%/%')[0]).participantId;
            console.log("===> JS onParticipantJoined <=== " + id + " " + subscriber.stream.connection);
            window.onParticipantJoined(id);

            // Add a new HTML element for the user's name and nickname over its video
            appendUserData(event.element, subscriber.stream.connection);
        });
    });

    session.on('publisherStartSpeaking', (event) => {
        let id = JSON.parse(event.target.connection.data.split('%/%')[0]).participantId;
        console.log("===> JS onParticipantStartedSpeaking <===");
        window.onParticipantStartedSpeaking(id);
    });

    session.on('publisherStopSpeaking', (event) => {
        let id = JSON.parse(event.target.connection.data.split('%/%')[0]).participantId;
        console.log("===> JS onParticipantStoppedSpeaking <===");
        window.onParticipantStoppedSpeaking(id);
    });

    // On every Stream destroyed...
    session.on('streamDestroyed', (event) => {
        let id = JSON.parse(event.stream.connection.data.split('%/%')[0]).participantId;
        console.log("===> JS onParticipantLeft <=== " + id);
        window.onParticipantLeft(id);
    });

    session.on('signal', (event) => {
        console.log(`===> JS event type: ${event.type}; data: ${event.data} <===`);
        window.onNewEvent(event.type, event.from, event.data);
    });

    // --- 4) Connect to the session passing the retrieved token and some more data from
    //        the client (in this case a JSON with the nickname chosen by the user) ---

    // var nickName = $("#nickName").val();
    session.connect(token, {participantId: participantId})
        .then(() => {

            // Here we check somehow if the user has 'PUBLISHER' role before
            // trying to publish its stream. Even if someone modified the client's code and
            // published the stream, it wouldn't work if the token sent in Session.connect
            // method is not recognized as 'PUBLIHSER' role by OpenVidu Server
            if (isPublisher) {

                // --- 6) Get your own camera stream ---

                publisher = openVidu.initPublisher('video-container', {
                    audioSource: undefined, // The source of audio. If undefined default microphone
                    videoSource: false,     // The source of video. If undefined default webcam
                    publishAudio: true,  	// Whether you want to start publishing with your audio unmuted or not
                    publishVideo: false,  	// Whether you want to start publishing with your video enabled or not// The frame rate of your video
                    insertMode: 'APPEND',	// How the video is inserted in the target element 'video-container'
                    mirror: false       	// Whether to mirror your local video or not
                });

                // --- 7) Specify the actions when events take place in our publisher ---

                // When our HTML video has been added to DOM...
                publisher.on('videoElementCreated', (event) => {
                    let id = participantId;//event.stream.connection.data['participantId'];
                    console.log("===> JS onJoined PUBLISHER <=== " + id);
                    window.onJoined(id);
                    $(event.element).prop('muted', true); // Mute local video
                });


                // --- 8) Publish your stream ---

                session.publish(publisher);

            } else {
                console.warn('You don\'t have permissions to publish');
            }
        })
        .catch(error => {
            console.warn('There was an error connecting to the session:', error.code, error.message);
        });

    return false;
}

function leaveSession() {
    session.disconnect();
    session = null;
}

/* APPLICATION BROWSER METHODS */

window.onbeforeunload = () => { // Gracefully leave session
    if (session) {
        leaveSession();
    }
};

function appendUserData(videoElement, connection) {
    let clientData;
    let serverData;
    let nodeId;
    console.log('///// ====> CONNECTION: ' + connection);
    if (connection.nickName) { // Appending local video data
        clientData = connection.nickName;
        serverData = connection.userName;
        nodeId = 'main-videodata';
    } else if (connection.data) {
        clientData = JSON.parse(connection.data.split('%/%')[0]).clientData;
        serverData = JSON.parse(connection.data.split('%/%')[1]).serverData;
        nodeId = connection.connectionId;
    } else {
        clientData = "Client data";
        serverData = "Server Data";
        nodeId = "NodeId";
    }
    console.log(`appendUserData ${connection.data}`);
}
