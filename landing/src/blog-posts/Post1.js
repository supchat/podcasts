import SyntaxHighlighter from "react-syntax-highlighter";
import {docco} from "react-syntax-highlighter/dist/cjs/styles/hljs";
import {FaFacebookSquare, FaReddit, FaTwitter} from "react-icons/fa";
import React from "react";
import {SimpleTopBar} from "../SimpleTopBar";

export const Post1 = () => {
    return (
        <main>
            <article>
                <section className="section-header pb-7 pb-lg-10 bg-primary text-white">
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-12 col-lg-10 text-center">
                                <h1 className="display-3 mb-4 px-lg-5">How to have a voice chat with your website
                                    visitors.</h1>
                                <div className="post-meta">
                                    <span className="font-weight-bold mr-3">Ivan Fytsyk</span>
                                    <span className="post-date mr-3">December 18, 2021</span>
                                    <span className="font-weight-bold">5 min read</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="pattern bottom"/>
                </section>
                <div className="section section-sm bg-white pt-5 text-black">
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-12 col-lg-8">
                                <p> As a website owner, you always fight for your visitors' attention.
                                    The traditional channels are blogs, videos, support chats. Everything has its
                                    pros
                                    and
                                    cons. Here I'll try to show you a new way how to communicate with your
                                    customers.</p>
                                <p>
                                    <img src="/assets/img/illustrations/blog/social-media.jpeg" alt="Blog Image"/>
                                </p>
                                <p>You can try our Podcasts widget. It allows you to schedule and start voice
                                    sessions
                                    with
                                    your website visitors. Also, it brings users back to your website. I'll show you
                                    how.</p>
                                <p>Integration of our widget is as easier as four lines of code.</p>
                                <p><SyntaxHighlighter language="javascript" style={docco}>
                                    {"<script src=\"https://widget.supcast.io/supcast-widget/v1.0/supcast.js\"></script>\n" +
                                    "<script>\n" +
                                    "    initializeSupcast(\"API-KEY\", \"https://supcast.io/\", \"light\");\n" +
                                    "</script>"}
                                </SyntaxHighlighter></p>
                                <p>Use your API that you received after registration. The second
                                    param <b>"https://supcast.io" </b>
                                    must be your website URL. It is used to redirect users back to your website when
                                    your
                                    podcast starts.</p>
                                <p>After integration, you'll see a grey button at the bottom right corner of your
                                    website.
                                    It means that no session is scheduled yet.
                                    <img src="/assets/img/illustrations/blog/blog1_screen1.png" alt="Blog Image"/>
                                </p>
                                <p>And now is the time of your first podcast. Login inside the widget using your
                                    credentials. After you'll be able to create a podcast. Define podcast topic,
                                    select
                                    date
                                    and time, and podcast is ready to start.
                                    <img src="/assets/img/illustrations/blog/blog1_screen2.png" alt="Blog Image"/>
                                </p>
                                <p>Once your customer opens your website he'll see a popup that something
                                    interesting is
                                    happening here.
                                </p>
                                <p><img src="/assets/img/illustrations/blog/blog1_screen3.png" alt="Blog Image"/>
                                </p>
                                <p>If a podcast has started, customers can instantly join it. Otherwise, he can put
                                    a
                                    reminder on his calendar. When the podcast start he'll receive a notification
                                    and
                                    get
                                    back to your website to join a podcast.</p>
                                <p><img src="/assets/img/illustrations/blog/blog1_screen4.png" alt="Blog Image"/>
                                </p>
                                <p>Now you know how integrate SupCast into your website. Best of luck!</p>
                            </div>
                        </div>
                        <div className="row justify-content-sm-center align-items-center py-3 mt-3">
                            <div className="col-12 col-lg-8">
                                <div className="row">
                                    <div className="col-9 col-md-6">
                                        <h6 className="font-weight-bolder d-inline mb-0 mr-3">Share:</h6>
                                        <a href="https://twitter.com/intent/tweet/?text=Integrate%20podcasts%20widget%20on%20your%20website.%20SupCast.&amp;url=https%3A%2F%2Fsupcast.io%2Fblog%2Fpost1"
                                           target={"_blank"}>
                                            <button
                                                className="btn btn-sm mr-3 btn-icon-only btn-pill btn-twitter d-inline"><span
                                                className="btn-inner-icon"><FaTwitter/></span></button>
                                        </a>
                                        <a href="https://facebook.com/sharer/sharer.php?u=https%3A%2F%2Fsupcast.io%2Fblog%2Fpost1"
                                           target={"_blank"}>
                                            <button
                                                className="btn btn-sm mr-3 btn-icon-only btn-pill btn-facebook d-inline"><span
                                                className="btn-inner-icon"><FaFacebookSquare/></span></button>
                                        </a>
                                        <a href="https://reddit.com/submit/?url=https%3A%2F%2Fsupcast.io%2Fblog%2Fpost1&amp;resubmit=true&amp;title=Integrate%20podcasts%20widget%20on%20your%20website.%20SupCast."
                                           target={"_blank"}>
                                            <button
                                                className="btn btn-sm btn-icon-only btn-pill btn-reddit d-inline"><span
                                                className="btn-inner-icon"><FaReddit/></span>
                                            </button>
                                        </a>
                                    </div>
                                    <div className="col-3 col-md-6 text-right">
                                        <i className="far fa-bookmark text-primary" data-toggle="tooltip"
                                           data-placement="top"
                                           title="" data-original-title="Bookmark story"/>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </main>
    );
}