export const Contact = () => {
    return (
        <main>
            <section className="section-header pb-7 pb-lg-11 bg-soft">
                <div className="section section-lg pt-0 line-bottom-light">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 col-md-6 text-center px-4 mb-5 mb-lg-0">
                                <div className="icon icon-sm icon-shape icon-shape-primary rounded mb-4">
                                    <i className="fas fa-envelope-open-text"/>
                                </div>
                                <h5 className="mb-3">Email us</h5>
                                <p>
                                    Email us for general queries, including marketing and partnership opportunities.
                                </p>
                                <a className="font-weight-bold text-primary" href="mailto://i@supchat.chat">i@supchat.chat</a>
                            </div>
                            <div className="col-12 col-md-6 text-center px-4 mb-5 mb-lg-0">
                                <div className="icon icon-sm icon-shape icon-shape-primary rounded mb-4">
                                    <i className="fas fa-phone-volume"/>
                                </div>
                                <h5 className="mb-3">Call us</h5>
                                <p>
                                    Call us to speak to a member of our team.We are always happy to help you.
                                </p>
                                <a className="font-weight-bold text-primary" href="tel://+380 98 83 03 320">+380 98 83 03 320</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    );
}