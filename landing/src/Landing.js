import React, {Component, useContext, useEffect, useRef, useState} from 'react';
import "./dashboard/scss/dashboard.scss"
import "./scss-2/front.scss"
import {AuthContext} from "./App";
import {SignUpForm} from "./SignUpForm";
import {LoginForm} from "./LoginForm";

const axios = require('axios');

const Arrow = ({onMount}) => {
    const [value, setValue] = useState(1.0);

    console.log("arrow value " + value);

    useEffect(() => {
        onMount([value, setValue]);
    }, [onMount, value]);

    let maxAngle = 60;
    let angle = maxAngle * (1 - value) - 10;
    return (<div
        className="col-6 col-md-6 text-center">
        <img src="assets/img/illustrations/supcast-arrow.svg" style={{
            transform: `rotate(${angle}deg)`,
            "transition-duration": "0.5s",
            "transition-property": "transform",
            "transition-timing-function": "cubic-bezier(.17,1.76,.74,.49)"

        }}/>
    </div>);
}

export const Landing = (props) => {
    const {auth, updateAuth} = useContext(AuthContext);
    const [state, updateState] = useState({...props, authState: 'signUp'});

    let switchAuthState = () => {
        let newState = state.authState === 'login' ? 'signUp' : 'login'
        updateState({...props, authState: newState});
    }

    const arrowRef = useRef();

    let arrowValue = null;
    let setArrowValue = null;
    const onArrowMount = (dataFromChild) => {
        arrowValue = dataFromChild[0];
        setArrowValue = dataFromChild[1];
    };


    useEffect(() => {
        const updateArrow = () => {
            if (arrowRef.current) {
                const current = arrowRef.current.getBoundingClientRect();
                let top = current.top;
                let screenHeight = window.innerHeight;

                if (top > 0 && top < screenHeight) {
                    setArrowValue(top / screenHeight);
                }

            }
        }

        updateArrow();

        window.onscroll = () => {
            updateArrow();
        }
    }, []);

    return (
        <main>
            <section className="section-header pb-7 pb-lg-11 bg-soft">
                <div className="container">
                    <div className="row justify-content-between align-items-center">
                        <div className="col-12 col-md-6 order-2 order-lg-1">
                            <img src="assets/img/illustrations/podcasts_preview.png" alt=""/>
                        </div>
                        <div className="col-12 col-md-5 order-1 order-lg-2">
                            <h1 className="display-2 mb-3">Talk to your users with our Podcasts widget.</h1>
                            <p className="lead">Improve your website bouncing rate, retention and promote your
                                service through
                                voice.</p>
                            {!props.userLogged && (state.authState === 'signUp') && SignUpForm(switchAuthState = {switchAuthState})}
                            {!props.userLogged && (state.authState === 'login') && LoginForm(switchAuthState = {switchAuthState})}
                        </div>
                    </div>
                </div>
                <div className="pattern bottom"/>
            </section>
            <section className="section section-lg pt-6">
                <div className="container">
                    <div className="row justify-content-center mb-5 mb-md-7">
                        <div className="col-12 col-md-8 text-center">
                            <h2 className="h1 font-weight-bolder mb-4">Try our podcasts widget</h2>
                            <p className="lead">Easy to integrate with your website. Low latency and high
                                quality
                                sound.</p>
                        </div>
                    </div>
                    <div className="row justify-content-center mb-5 mb-md-7" ref={arrowRef}>
                        <Arrow onMount={onArrowMount}/>
                    </div>
                </div>
            </section>

            <section className="section section-lg pb-5 bg-soft">
                <div className="container">
                    <div className="row">
                        <div className="col-12 text-center mb-5">
                            <h2 className="mb-4">Better communication with your users</h2>
                            <p className="lead mb-5">Join us </p>
                        </div>
                        <div className="col-12 text-center">
                            <button className="btn btn-secondary animate-up-2" data-toggle="modal"
                                    data-target=".pricing-modal">
                                <span className="mr-2"><i className="fas fa-hand-pointer"/></span>Pricing
                            </button>
                        </div>
                    </div>
                </div>
            </section>

        </main>
    );

}
