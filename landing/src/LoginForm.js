import {getAuth, signInWithEmailAndPassword} from "firebase/auth";
import {AuthContext} from "./App";
import {useContext} from "react";
import {saveUserData, showError, showProgress, showSuccess, showWarning} from "./helpers";

const axios = require('axios');

export const LoginForm = (props) => {
    const {auth, updateAuth} = useContext(AuthContext);

    async function signIn(event) {
        event.preventDefault();
        showProgress("Please wait. We are creating your account.")

        let email = event.target.elements.email.value;
        let password = event.target.elements.password.value;

        const auth = getAuth();

        signInWithEmailAndPassword(auth, email, password)
            .then(async (userCredential) => {
                showSuccess('You successfully logged in', 'Move to Dashboard');
                const user = userCredential.user;
                let token = await user.getIdToken(true)

                let hostData = await axios.get('https://supcast.io/getHost',
                    {
                        headers: {
                            'Authorization': token,
                        }
                    }
                )
                saveUserData(hostData.data);
                updateAuth();
            })
            .catch((error) => {
                const errorCode = error.code;
                const errorMessage = error.message;
                showError('Error while logging in');
            });

    }


    return <div id="sign-up-form-wrapper" className="mt-4">
        <form onSubmit={signIn} id="sign-up-form"
              className="d-flex flex-column mb-5 mb-lg-0">
            <input className="form-control mt-2" type="email" name="email"
                   placeholder="Email"
                   required/>
            <input className="form-control my-2" type="password" name="password"
                   placeholder="Password"
                   required/>
            <button className="btn btn-primary" type="submit">Login to your account</button>

            <div
                className="d-block d-sm-flex justify-content-center align-items-center mt-4">
                                <span className="font-weight-normal">
                                    Don't have an account?
                                    <a href="#" onClick={props.switchAuthState}
                                       className="font-weight-bold"> Register</a>
                                </span>
            </div>
        </form>
    </div>

}