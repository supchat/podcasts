import {getAuth, signOut} from "firebase/auth";
import {AuthContext} from "./App";
import {useContext} from "react";
import {goToHome, clearUserData, showError} from "./helpers";

export const TopBarLogout = () => {
    const {authUser, updateAuth} = useContext(AuthContext);
    const logout = () => {
        const auth = getAuth();
        signOut(auth).then(() => {
            clearUserData();
            updateAuth();
            goToHome();
        }).catch((error) => {
            showError('We cannot sign you out ...');
        });
    }

    return (
        <div>
            <a onClick={logout}>
            <div className="media-body mr-3 d-none d-lg-block">
                <span  className="mb-0 text font-weight-bold">Sign out</span>
            </div>
            </a>
        </div>
    )
}
