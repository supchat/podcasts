export const Footer = () => {
    return (
        <footer className="footer section pt-6 pt-md-8 pt-lg-10 pb-3 bg-primary text-white overflow-hidden">
            <div className="pattern pattern-soft top"/>
            <div className="container">
                <div className="row">
                    <div className="col-lg-4 mb-4 mb-lg-0">
                        <a className="footer-brand mr-lg-5 d-flex" href="/">
                            <img src="assets/img/brand/SupCast-light.svg" height="35" className="mr-3"
                                 alt="Footer logo"/>
                        </a>
                        <p className="my-4">Integrate SupCast widget with your website and start talking with your
                            customers.</p>
                    </div>

                </div>
                <hr className="my-4 my-lg-5"/>
                <div className="row">
                    <div className="col pb-4 mb-md-0">
                        <div className="d-flex text-center justify-content-center align-items-center">
                            <p className="font-weight-normal mb-0">Copyright © 2021 <a
                                href="/">SupCast</a> <span
                                className="current-year"/>.
                                All rights reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    );
}