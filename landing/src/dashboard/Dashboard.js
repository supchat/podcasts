import React, {Component, useState} from 'react';
import {FaRegCopy} from "react-icons/fa";
import {IconContext} from "react-icons";
import {savePhotoUrl, saveUserName, showProgress, showSmallInfo, showSuccess} from "../helpers";
import Gist from "react-gist";
import {getStorage, ref, uploadBytes, getDownloadURL} from "firebase/storage";
import {getAuth, onAuthStateChanged} from "firebase/auth";
import {doc, setDoc} from "firebase/firestore";
import {getFirestore} from "firebase/firestore"
import {docco} from "react-syntax-highlighter/dist/cjs/styles/hljs";
import SyntaxHighlighter from "react-syntax-highlighter";


export const Dashboard = () => {

    let userId = localStorage.getItem("userId");
    let userName = localStorage.getItem("userName");
    let photoUrl = localStorage.getItem("photoUrl");
    let clientId = localStorage.getItem("clientId");

    const [state, updateState] = useState({
        showHowUse: false,
        pickedImage: null,
        updateButtonEnabled: false,
        updatedName: null,
        rawPickedImage: null
    })

    const copyCode = () => {
        navigator.clipboard.writeText(clientId);
        showSmallInfo('API key copied to clipboard!');
    }

    const toggleTutorial = () => {
        updateState({showHowUse: !state.showHowUse});
    }

    const handleFileInput = (event) => {
        if (event.target.files.length === 0) {
            return;
        }
        let rawPickedImage = event.target.files[0];
        let file = URL.createObjectURL(rawPickedImage);
        updateState({
            showHowUse: state.showHowUse,
            pickedImage: file,
            updateButtonEnabled: (name && name !== userName) || (file),
            updatedName: state.updatedName,
            rawPickedImage: rawPickedImage
        });
    }

    const onNameChanged = (event) => {
        let name = event.target.value;

        updateState({
            updatedName: name,
            updateButtonEnabled: (name && name !== userName) || (state.pickedImage),
            showHowUse: state.showHowUse,
            pickedImage: state.pickedImage,
            rawPickedImage: state.rawPickedImage
        });
    }

    const generateRandomFileName = () => {
        return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    }

    const updateUserData = () => {
        showProgress("Please wait.");

        const auth = getAuth();
        onAuthStateChanged(auth, (user) => { // upload files does not work if we don't add this check
            if (user) {
                const uid = user.uid;
                console.log("==== user logged");
            } else {
                // User is signed out
                console.log("==== user NOT logged");
            }
        });

        let filePath = `${clientId}/${userId}/profilePicture/${generateRandomFileName()}.jpeg`

        const storage = getStorage();
        const storageRef = ref(storage, filePath);

        const metadata = {
            contentType: 'image/jpeg',
        };

        const db = getFirestore();
        const documentPath = doc(db, `clients/${clientId}/hosts/${userId}`);

        if (state.updatedName) {
            const data = {
                name: state.updatedName
            }
            setDoc(documentPath, data, {merge: true});
            saveUserName(state.updatedName);
            userName = state.updatedName;
        }

        if (state.rawPickedImage)
            uploadBytes(storageRef, state.rawPickedImage, metadata).then((snapshot) => {
                getDownloadURL(snapshot.ref).then((downloadURL) => {

                    const data = {
                        photoUrl: downloadURL
                    }

                    setDoc(documentPath, data, {merge: true});

                    console.log(`File available at, userID: ${userId}`, downloadURL);
                    savePhotoUrl(downloadURL);
                    photoUrl = downloadURL;
                });
            }).catch((error) => {
                console.log(error);
            });

        updateState({
            showHowUse: state.showHowUse,
            pickedImage: state.pickedImage,
            updateButtonEnabled: false,
            updatedName: state.updatedName,
            rawPickedImage: state.rawPickedImage
        });
    }

    return (
        <main>
            <div className="section-header pb-7 pb-lg-11 bg-soft">
                <div className="container">
                    <div className="row">
                        <div className="pl-0 col-xl-6 col-md-6">
                            <div className="card bg-gradient-primary border-0">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col">
                                            <h5 className="card-title text-uppercase text-muted mb-0 text-white">API
                                                Key</h5>
                                            <span id="api-key"
                                                  className="h2 font-weight-bold mb-0 text-white">{clientId}</span>
                                        </div>
                                        <a onClick={copyCode}>
                                            <IconContext.Provider
                                                value={{
                                                    color: "white",
                                                    style: {padding: '10px', height: '60px', width: '60px'}
                                                }}>
                                                <FaRegCopy/>
                                            </IconContext.Provider>
                                        </a>
                                    </div>
                                    <p className="mt-3 mb-0 text-sm" onClick={toggleTutorial}>
                                        <a href="#!" className="text-nowrap text-white font-weight-600">See how to
                                            use
                                            it</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="card col-xl-6">
                            <div className="card-header">
                                <div className="row align-items-center">
                                    <div className="col-8">
                                        <h3 className="mb-0">Edit profile </h3>
                                    </div>
                                    <div className="col-4 text-right">
                                        <a href="#!" onClick={updateUserData}
                                           className={`btn btn-sm btn-primary ${(state.updateButtonEnabled) ? "" : "disabled"}`}>Update</a>
                                    </div>
                                </div>
                            </div>
                            <div className="card-body">
                                <form>
                                    <h6 className="heading-small text-muted mb-4">Host information</h6>
                                    <div>
                                        <div className="row">
                                            <div className="col-9">
                                                <div className="form-group">
                                                    <label className="form-control-label" htmlFor="input-username">Host
                                                        name</label>
                                                    <input type="text" id="input-username" className="form-control"
                                                           placeholder="Username" value={state.updatedName || userName}
                                                           onChange={onNameChanged}/>
                                                </div>
                                            </div>
                                            <div className="card-profile-image col-1">
                                                <div>
                                                    <label htmlFor="file">
                                                        <img src={state.pickedImage || photoUrl}
                                                             className="rounded-circle"/>
                                                    </label>
                                                    <input id="file" type="file" name="myImage"
                                                           onChange={handleFileInput}
                                                           hidden accept="image/*"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                    {state.showHowUse && <div className="row">
                        <div className="col-12 col-lg-12 px-0">
                            <div className="card">
                                <div className="card-body">
                                    <h3
                                        className="card-text mb-4">
                                        <h4>Installing SupCast is very simple, it's going to take only a few moments to
                                            get
                                            your
                                            website powered with our podcast widget.</h4>
                                        <ol>
                                            <li>Log in to your SupCast account and go to Dashboard</li>
                                            <li>Copy your <span className={"text-success"}>API key</span> to the
                                                clipboard
                                            </li>
                                            <li>Add the following code before the closing {"body"} tag of your website
                                                <ul>
                                                    <li>replace <span
                                                        className={"text-success"}>API-KEY</span> with your key
                                                    </li>
                                                    <li>replace <span
                                                        className={"text-success"}>https://supcast.io/</span> with your
                                                        website address, this
                                                        address will
                                                        be used in
                                                        calendar events to navigate users who added upcoming broadcasts
                                                        to
                                                        their
                                                        calendars
                                                    </li>
                                                    <li>
                                                        also, you can choose between two widget appearances: <span
                                                        className={"text-success"}>light</span> and
                                                        <span className={"text-success"}> dark</span>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li> Once you’ve successfully added SupCast code to your website, refresh
                                                the
                                                page and you
                                                should see our podcast button in the bottom right corner.
                                                That's it, you are ready to go now.

                                                If you have any issues with the installation, contact our support.
                                            </li>
                                        </ol>
                                    </h3>
                                    <p><SyntaxHighlighter language="javascript" style={docco}>
                                        {"<script src=\"https://widget.supcast.io/supcast-widget/v1.0/supcast.js\"></script>\n" +
                                        "<script>\n" +
                                        "    initializeSupcast(\"API-KEY\", \"https://your-website.xyz/\", \"light\");\n" +
                                        "</script>"}
                                    </SyntaxHighlighter></p></div>
                            </div>
                        </div>
                    </div>}
                </div>
            </div>
        </main>
    );
}