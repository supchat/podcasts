import React, {Component} from 'react';
import placeHolder from './assets/profile_placeholder.svg'
import {goToDashboard} from "./helpers";

export const TopBarAvatar = () => {
    let userName = localStorage.getItem("userName");
    let photoUrl = localStorage.getItem("photoUrl");
    return (
        <div>
            <a className="nav-link pr-0" href="/landing-profile" role="button">
                <div className="media align-items-center">
                    <div className="media-body mr-3 d-none d-lg-block">
                        <span id="top-profile-name"
                            className="mb-0 text font-weight-bold">Go to dashboard</span>
                    </div>
                    <span className="avatar avatar-sm rounded-circle">
                        <img alt="Image placeholder" src={photoUrl || placeHolder}/>
                    </span>
                </div>
            </a>
        </div>
    )
}
