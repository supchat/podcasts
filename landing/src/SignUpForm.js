import {saveUserData, showError, showProgress, showSuccess, showWarning} from "./helpers";
import {AuthContext} from "./App";
import {useContext} from "react";

const axios = require('axios');

export const SignUpForm = (props) => {

    const {auth, updateAuth} = useContext(AuthContext);

    async function signUp(event) {
        event.preventDefault();
        showProgress("Please wait. We are creating your account.")

        let termsAccepted = event.target.elements.agreeWithTermsCheckBox.checked;

        if (!termsAccepted) {
            showWarning("Please accept Terms And Conditions.")
            return;
        }
        let name = event.target.elements.name.value
        let email = event.target.elements.email.value
        let password = event.target.elements.password.value
        console.log(`${name} ${email} ${password}`)

        try {
            let result = await axios.post('https://supcast.io/hostRegister',
                {
                    "name": name,
                    "email": email,
                    "password": password
                });

            if (result.data.userId) {
                saveUserData(result.data);
                updateAuth();
                showSuccess('You successfully created an account', 'Move to Dashboard')
            } else {
                showError('We cannot create your account ...');
            }

        } catch (error) {
            showError('We cannot create your account ...');
        }
    }

    return <div id="sign-up-form-wrapper" className="mt-4">
        <form onSubmit={signUp} id="sign-up-form"
              className="d-flex flex-column mb-5 mb-lg-0">
            <input className="form-control" type="text" name="name"
                   placeholder="Your name"
                   required/>
            <input className="form-control mt-2" type="email" name="email"
                   placeholder="Email"
                   required/>
            <input className="form-control my-2" type="password" name="password"
                   placeholder="Password"
                   required/>
            <button className="btn btn-primary" type="submit">Create your account</button>

            <div className="form-group form-check mt-3">
                <input type="checkbox" className="form-check-input"
                       id="agreeWithTermsCheckBox"/>
                <label
                    className="form-check-label form-check-sign-white"
                    htmlFor="agreeWithTermsCheckBox">I agree to the
                    <a href="/landing-terms"> Terms & Conditions</a></label>
            </div>

            <div
                className="d-block d-sm-flex justify-content-center align-items-center mt-4">
                                <span className="font-weight-normal">
                                    Already have an account?
                                    <a href="#" onClick={props.switchAuthState} className="font-weight-bold"> Login</a>
                                </span>
            </div>
        </form>
    </div>
}