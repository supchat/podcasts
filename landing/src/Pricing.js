
export const Pricing = () => {
    return (
        <div id="pricing-modal" className="modal fade pricing-modal" tabIndex="-1" role="dialog"
             aria-hidden="true">
            <div className="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div className="modal-content py-4">
                    <div className="px-3">
                        <div className="col-12 d-flex justify-content-end d-lg-none">
                            <i className="fas fa-times" data-dismiss="modal" aria-label="Close"></i>
                        </div>
                    </div>
                    <div className="modal-header text-center text-black">
                        <div className="col-12">
                            <h4 className="px-lg-6">Our 30-days trial gives you full access to all tools and
                                features of your
                                chosen plan.</h4>
                        </div>
                    </div>
                    <div className="modal-body">
                        <div className="row">
                            <div className="col-12 col-lg-6 text-left">
                                <div className="form-check card border-light p-3">
                                    <input className="form-check-input" type="radio" name="exampleRadios" onChange={() => {}}
                                           id="exampleRadios1"
                                           value="option1" checked/>
                                    <label className="form-check-label" htmlFor="exampleRadios1">
                                        <span className="h6 text-black d-block">Free</span>
                                        <span className="small text-gray">Try it for free.</span>
                                        <span className="text-gray mt-3 d-block p">Upgrade your website with our free podcasts widget.</span>
                                    </label>
                                </div>
                            </div>
                            <div className="col-12 col-lg-6 text-left">
                                <div className="form-check  card border-light p-3">
                                    <input className="form-check-input" type="radio" name="exampleRadios" onChange={() => {}}
                                           id="exampleRadios2"
                                           value="option2" readOnly/>
                                    <label className="form-check-label" htmlFor="exampleRadios2">
                                        <span className="h6 text-black d-block">Pro</span>
                                        <span className="small text-gray">Coming soon</span>
                                        <span className="text-gray mt-3 d-block p">Advanced features: statistics, unlimited users, fast support.</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="modal-footer border-0 text-center">
                        <div className="col text-gray">
                            <a href="/" className="btn btn-primary mb-4">Continue</a>
                            <p className="small mb-0">You can upgrade, downgrade, or cancel your
                                subscription
                                anytime.<br/>No
                                contracts, no hidden charges.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}