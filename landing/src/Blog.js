import React from "react";

export const Blog = () => {
    return (
        <div>
            <section className="section-header bg-primary text-white pb-7 pb-lg-11">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-12 col-md-8 text-center">
                            <h1 className="display-2 mb-4">The SupCast Blog</h1>
                            <p className="lead">We help you to be more in touch with your customers. Talk, Ask, Promote
                                your goods using our podcasts widget.</p>
                        </div>
                    </div>
                </div>
                <div className="pattern bottom"/>
            </section>
            <section className="section section-lg line-bottom-light">
                <div className="container mt-n7 mt-lg-n12 z-2">
                    <div className="row">
                        <div className="col-lg-12 mb-5">
                            <div className="card bg-white border-light shadow-soft flex-md-row no-gutters p-4">
                                <a href="/landing-blog/post1" className="col-md-6 col-lg-6"><img
                                    src="/assets/img/illustrations/podcasts_preview.png" alt=""
                                    className="card-img-top"/></a>
                                <div
                                    className="card-body d-flex flex-column justify-content-between col-auto py-4 p-lg-5">
                                    <a href="./landing-blog/post1">
                                        <h2>How to have a voice chat with your website visitors.</h2>
                                    </a>
                                    <p>As a website owner, you always fight for your visitors' attention.
                                        The traditional channels are blogs, videos, support chats. Everything has its
                                        pros and cons. Here I'll try to show you a new way how to communicate with your
                                        customers.</p>
                                    <div className="d-flex align-items-center">
                                        <img className="avatar avatar-sm rounded-circle"
                                             src="/assets/img/illustrations/ic_ivan.png" alt=""/>
                                        <h6 className="text-muted small ml-2 mb-0">Ivan Fytsyk</h6>
                                        <h6 className="text-muted small font-weight-normal mb-0 ml-auto">
                                            <time dateTime="2021-04-25">18 December, 2021</time>
                                        </h6>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
            <section className="section section-lg pb-5 bg-soft"/>

        </div>
    )
}