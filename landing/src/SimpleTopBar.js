
import lightLogo from "./assets/brand/SupCast.svg"
import darkLogo from "./assets/brand/SupCast-light.svg"

export const SimpleTopBar = () => {
    return (<header className="header-global">
        <nav id="navbar-main"
             className="navbar navbar-main navbar-expand-lg headroom py-lg-3 px-lg-6 navbar-dark navbar-theme-primary">
            <div className="container">
                <a className="navbar-brand @@logo_classes" href="/">
                    <img className="navbar-brand-light common" src={lightLogo} height="35"
                         alt="Logo light"/>
                    <img className="navbar-brand-dark common" src={darkLogo}
                         height="35"
                         alt="Logo light"/>
                </a>
                <div className="navbar-collapse collapse" id="navbar_global">
                    <div className="navbar-collapse-header">
                        <div className="row">
                            <div className="col-6 collapse-brand">
                                <a href="/">
                                    <img src={lightLogo} height="35" alt="Logo SupCast"/>
                                </a>
                            </div>
                            <div className="col-6 collapse-close">
                                <a href="#navbar_global" role="button" className="fas fa-times"
                                   data-toggle="collapse"
                                   data-target="#navbar_global" aria-controls="navbar_global"
                                   aria-expanded="false"
                                   aria-label="Toggle navigation"/>
                            </div>
                        </div>
                    </div>
                    <ul className="navbar-nav navbar-nav-hover justify-content-center">
                        <li className="nav-item">
                            <a href="#" className="nav-link" data-toggle="modal"
                               data-target=".pricing-modal">Pricing</a>
                        </li>
                        <li className="nav-item">
                            <a href="/landing-contact" className="nav-link">Contact Us</a>
                        </li>
                        <li className="nav-item">
                            <a href="/landing-blog" className="nav-link">Blog</a>
                        </li>
                    </ul>
                </div>
                <div className="d-flex d-lg-none align-items-center">
                    <button className="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbar_global"
                            aria-controls="navbar_global" aria-expanded="false"
                            aria-label="Toggle navigation"><span
                        className="navbar-toggler-icon"/></button>
                </div>
            </div>
        </nav>
    </header>);
}