import Swal from 'sweetalert2'

export function showProgress(text) {
    Swal.fire({
        title: "Progress...",
        text: text,
        timer: 2000,
        timerProgressBar: true,
        showConfirmButton: false,
        allowOutsideClick: true
    });
}

export function showWarning(text) {
    Swal.fire({
        text: text,
        icon: 'info',
        showConfirmButton: true,
        allowOutsideClick: true
    });
}

export function showError(text) {
    Swal.fire({
        title: 'Error',
        text: text,
        icon: 'warning',
        confirmButtonColor: 'RoyalBlue',
    })
}

export function showSmallInfo(text) {
    Swal.fire({
        text: text,
        confirmButtonColor: 'RoyalBlue',
    });
}

export function showSuccess(text, buttonText, onButtonClick) {
    Swal.fire({
        title: 'Success',
        text: text,
        icon: 'success',
        confirmButtonText: buttonText,
        confirmButtonColor: 'RoyalBlue',
    }).then((result) => {
        if (result.isConfirmed) {
            goToDashboard();
        }
    })
}

export function saveUserData(userData) {
    let clientId = userData.clientId;
    let userId = userData.userId;
    let name = userData.name;
    let photoUrl = userData.photoUrl;

    localStorage.setItem("clientId", clientId);
    localStorage.setItem("userId", userId);
    localStorage.setItem("userName", name);
    localStorage.setItem("photoUrl", photoUrl);
}

export function clearUserData() {
    localStorage.removeItem("clientId");
    localStorage.removeItem("userId");
    localStorage.removeItem("userName");
    localStorage.removeItem("photoUrl");
}

export function saveUserName(name) {
    if (name) {
        localStorage.setItem("userName", name);
    }
}

export function savePhotoUrl(photoUrl) {
    if (photoUrl) {
        localStorage.setItem("photoUrl", photoUrl);
    }
}

export function goToDashboard() {
    window.open("/landing-profile", "_self")
}

export function goToHome() {
    window.open("/", "_self")
}
