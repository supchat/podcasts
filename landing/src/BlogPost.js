import React from "react";
import {
    useParams
} from "react-router-dom";

import {Post1} from "./blog-posts/Post1";
import {SimpleTopBar} from "./SimpleTopBar";

export const BlogPost = () => {
    let {topicId} = useParams();

   switch (topicId) {
       case "post1": return (<Post1/>);
       default:  return (<Post1/>);
   }


}