import React, {Component, useState} from 'react';
import "./dashboard/scss/dashboard.scss"
import {TopBar} from "./TopBar";
import {Landing} from "./Landing";
import {Dashboard} from "./dashboard/Dashboard";

import {
    BrowserRouter as Router,
    Switch,
    Route,
    useRouteMatch,
    Link
} from "react-router-dom";
import {initializeApp} from "firebase/app";
import {getAnalytics} from "firebase/analytics";
import {Terms} from "./Terms";
import {Contact} from "./Contact";
import {Pricing} from "./Pricing";
import {Footer} from "./Footer";
import {Blog} from "./Blog";
import {BlogPost} from "./BlogPost";
import {SimpleTopBar} from "./SimpleTopBar";

const firebaseConfig = {
    apiKey: "AIzaSyBc1yPiHY7gH7C0NvBMU1TUeXfYjKRSzY0",
    authDomain: "podcasts-d312e.firebaseapp.com",
    projectId: "podcasts-d312e",
    storageBucket: "podcasts-d312e.appspot.com",
    messagingSenderId: "794460114980",
    appId: "1:794460114980:web:f7157c094b6885023448b1",
    measurementId: "G-GK4PMV048L"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

export const AuthContext = React.createContext();

export const App = () => {

    let getLocalUserData = () => {
        let userId = localStorage.getItem("userId");
        let userName = localStorage.getItem("userName");
        let photoUrl = localStorage.getItem("photoUrl");
        let clientId = localStorage.getItem("clientId");

        return {userId, userName, photoUrl, clientId};
    }

    let createState = () => {
        let {userId, userName, photoUrl, clientId} = getLocalUserData();
        return {
            auth: {
                userId: userId,
                userName: userName,
                photoUrl: photoUrl,
                clientId: clientId
            }
        };
    }

    const [appState, updateAppState] = useState(createState())

    function updateAuth() {
        updateAppState(createState())
    }

    let auth = appState.auth;

    return <AuthContext.Provider value={{auth, updateAuth}}>
        <Router>
            <Switch>
                <Route exact path="/">
                    <NavBarContainer userLoggedIn={auth.userId != null}>
                        <Landing/>
                    </NavBarContainer>
                </Route>
                <Route path="/landing-profile">
                    <NavBarContainer displayLogout={true}>
                        <Dashboard/>
                    </NavBarContainer>
                </Route>
                <Route path="/landing-terms">
                    <NavBarContainer>
                        <Terms/>
                    </NavBarContainer>
                </Route>
                <Route path="/landing-contact">
                    <NavBarContainer>
                        <Contact/>
                    </NavBarContainer>
                </Route>
                <Route path="/landing-blog">
                    <BlogContainer/>
                </Route>
            </Switch>
        </Router>
    </AuthContext.Provider>


}

const BlogContainer = () => {
    let {path, url} = useRouteMatch();

    return (
        <div>
            <Pricing/>
            <SimpleTopBar/>
            <Switch>
                <Route path={`${path}/:topicId`}>
                    <BlogPost/>
                </Route>
                <Route exact path="/landing-blog">
                    <Blog/>
                </Route>
            </Switch>
        </div>
    )
}

const NavBarContainer = (props) => {

    let {path, url} = useRouteMatch();

    let options = {
        userLogged: props.userLoggedIn,
        displayLogout: props.displayLogout,
    };

    return (
        <div>
            <header className="header-global">
                <TopBar options={options}/>
            </header>
            {props.children}
            <Footer/>
            <Pricing/>
        </div>
    )
}
